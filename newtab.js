var bms = browser.bookmarks;
var grid = document.getElementById('grid');
var db = browser.storage.sync;

const SD_KEY = 'speed-dial-folders';

function getCard(name) {
    return `./cards/${name}.svg`;
}

function loadFavoritesFolder(id) {
    bms.getChildren(
        id
    ).then(bm_res => {
        fetch('./known_websites.csv').then(
            res => { if (res.ok) return res.text(); }
        ).then(known_websites => {
            known_websites = known_websites.trim();
            var kw_urls = known_websites.split('\n').map(row => row.split(';')[0]);
            var kw_dict = {};
            for (let row of known_websites.split('\n')) {
                row_split = row.split(';');
                kw_dict[row_split[0]] = row_split[1];
            }
            bm_res.filter(bm => bm.type === 'bookmark').forEach(bm => {
                let item = document.createElement('li');
                let host = new URL(bm.url).host;
                let matches = kw_urls.filter(kw => host.includes(kw));
                if (matches.length > 0) {
                    let card_img = getCard(kw_dict[matches[0]]);
                    item.style.backgroundImage = `url('${card_img}')`
                }
                else {
                    let icon = document.createElement('img');
                    icon.onerror = () => {icon.src = 'default_favicon.svg'}
                    icon.src = `https://external-content.duckduckgo.com/ip3/${host}.ico`;
                    item.appendChild(icon);
                }
                let span = document.createElement('span');
                span.appendChild(document.createTextNode(bm.title));
                item.appendChild(span);
                item.onclick = () => {location.href = bm.url;};
                grid.appendChild(item);
            });
        });
    });
}

function populateSettings(folders) {
    let list = document.getElementById('speed-dial-folders');
    list.innerHTML = '';
    folders.map(folder => {
        let fi = document.createElement('li');
        let span = document.createElement('span');
        span.appendChild(document.createTextNode(folder));
        fi.appendChild(span);
        let del_btn = document.createElement('button');
        del_btn.appendChild(document.createTextNode('×'));
        del_btn.onclick = () => {
            db.get({'speed-dial-folders': []}).then(res => {
                let n_folders = res[SD_KEY].filter(x => x != folder);
                db.set({'speed-dial-folders': n_folders}).then(res => {
                    populateAll();
                });
            });
        };
        fi.appendChild(del_btn);
        list.appendChild(fi);
    });
}

function populateAll() {
    grid.innerHTML = '';
    db.get({'speed-dial-folders': []}).then(res => {
        let folders = [];
        if (res[SD_KEY].length <= 0) {
            db.set({'speed-dial-folders': ['Speed Dial']});
            folders = ['Speed Dial'];
        }
        else {
            folders = res[SD_KEY];
        }
        populateSettings(folders);
        folders.map(t => bms.search({title: t}).then(res => {
            res.map(folder => loadFavoritesFolder(folder.id));
        }));
    });
}

function toggleSettings() {
    let settings = document.getElementById('settings');
    if (settings.className.includes('open')) {
        settings.classList.remove('open');
    }
    else {
        settings.classList.add('open');
    }
}

populateAll();

document.getElementById('settings-toggle').onclick = toggleSettings;
document.getElementById('new-folder-btn').onclick = () => {
    let n_folder_input = document.getElementById('new-folder-input');
    let n_folder = n_folder_input.value.trim();
    n_folder_input.value = '';
    if (!n_folder) return;
    db.get({'speed-dial-folders': []}).then(res => {
        db.set({'speed-dial-folders': [...res[SD_KEY], n_folder]}).then(res => {
            populateAll();
        });
    });
};
